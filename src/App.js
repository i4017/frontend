import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Start from './Start';
import Login from './Login';
import ForgotPassword from './ForgotPassword';
import ChooseAccountType from './ChooseAccountType';
import CreateAccountRefugee from './CreateAccountRefugee';
import CreateAccountHelper from './CreateAccountHelper';
import TestApi from './TestApi';
import MainPageRefugee from './MainPageRefugee';
import MainPageHelper from './MainPageHelper';
import ProfileHelper from './ProfileHelper';
import ProfileRefugee from './ProfileRefugee';
import MakeAccomodationOffer from './MakeAccomodationOffer';
import MakeMealOffer from './MakeMealOffer';
import FindAccommodation from './FindAccommodation';
import FindMeal from './FindMeal';
function App() {
  return (
    <BrowserRouter>
          <Routes>
            <Route index element={<Start/>}/>
            <Route path="Login" element={<Login/>}/>
            <Route path="Login/TestApi" element={<TestApi/>}/>
            <Route path="Login/ForgotPassword" element={<ForgotPassword/>}/>
            <Route path="Login/ChooseAccountType" element={<ChooseAccountType/>}/> 
            <Route path="Login/ChooseAccountType/CreateAccountRefugee" element={<CreateAccountRefugee/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountHelper" element={<CreateAccountHelper/>}/> 
            <Route path="Login/ChooseAccountType/CreateAccountRefugee/MainPageRefugee" element={<MainPageRefugee/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountHelper/MainPageHelper" element={<MainPageHelper/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountHelper/MainPageHelper/ProfileHelper" element={<ProfileHelper/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountRefugee/MainPageRefugee/ProfileRefugee" element={<ProfileRefugee/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountHelper/MainPageHelper/MakeAccomodationOffer" element={<MakeAccomodationOffer/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountHelper/MainPageHelper/MakeMealOffer" element={<MakeMealOffer/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountRefugee/MainPageRefugee/FindAccommodation" element={<FindAccommodation/>}/>
            <Route path="Login/ChooseAccountType/CreateAccountRefugee/MainPageRefugee/FindMeal" element={<FindMeal/>}/>

            <Route path="Login/MainPageRefugee" element={<MainPageRefugee/>}/>
            <Route path="Login/MainPageHelper" element={<MainPageHelper/>}/>
            <Route path="Login/MainPageHelper/ProfileHelper" element={<ProfileHelper/>}/>
            <Route path="Login/MainPageRefugee/ProfileRefugee" element={<ProfileRefugee/>}/>
            <Route path="Login/MainPageHelper/MakeAccomodationOffer" element={<MakeAccomodationOffer/>}/>
            <Route path="Login/MainPageHelper/MakeMealOffer" element={<MakeMealOffer/>}/>
            <Route path="Login/MainPageRefugee/FindAccommodation" element={<FindAccommodation/>}/>
            <Route path="Login/MainPageRefugee/FindMeal" element={<FindMeal/>}/>
          </Routes>
        </BrowserRouter>
  );
}

export default App;
