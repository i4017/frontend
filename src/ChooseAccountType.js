import { useNavigate } from 'react-router-dom';

function ChooseAccountType() {

  const navigate = useNavigate();

  const goToCreateAccountHelper = () => {
    navigate('CreateAccountHelper', { replace: false })
  }

  const goToCreateAccountRefugee = () => {
    navigate('CreateAccountRefugee', { replace: false })
  }

    return(
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Choose Account Type</h2>
          </div>

        <div class="flex justify-center items-center v-screen pt-20">
          <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-12 rounded mr-10"
          onClick={goToCreateAccountRefugee}>
            Refugee
          </button>

          <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-12 rounded ml-10"
          onClick={goToCreateAccountHelper}>
            Helper
          </button>
          </div>
        </div>
      </div>
    )
}

export default ChooseAccountType;