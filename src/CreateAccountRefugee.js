import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import { auth } from "./firebase-config";

function CreateAccountRefugee() {

  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");

  const [registerName, setRegisterName] = useState("");
  const [registerAge, setRegisterAge] = useState("");
  const [registerCountry, setRegisterCountry] = useState("");
  const [registerphone, setRegisterPhone] = useState("");

  const navigate = useNavigate();

  const goToMainPageRefugee = () => {
    navigate('MainPageRefugee', { replace: false })
  }

  const register = async () => {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      );
      console.log(user);


      /*baza de date Users
      - id
      - accountType
      - name
      - age
      - place
      - phone
      - email*/
      let accountType = "refugee"
      let name = registerName
      let age = registerAge
      let place = registerCountry
      let phone = registerphone
      let email = registerEmail
      //TODO baga in baza de date Users
      var jsonData = {
        "accountType" : accountType,
        "name" : name,
        "age" : age,
        "place" : place,
        "phone" : phone,
        "email" : email
      }
      fetch('/test/create_user', {  

      method: 'POST', 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(jsonData) 

    }).then(goToMainPageRefugee())

      
    } catch (error) {
      console.log(error.message);
    }
  };

    return(
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Create Account</h2>
          </div>

          <div class="flex justify-center items-center v-screen pt-10">
          
            <div className="mr-10">
                <label htmlFor="username" className="sr-only">
                  FirstLastName
                </label>
                <input
                  id="first-last-name"
                  name="name"
                  type="text"
                  autoComplete="name"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="First and Last Name"
                  onChange={(event) => {
                    setRegisterName(event.target.value);
                  }}
                />
            </div>
            <div className="ml-10">
                <label htmlFor="age" className="sr-only">
                  Age
                </label>
                <input
                  id="age"
                  name="age"
                  type="number"
                  autoComplete="age"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="Age"
                  onChange={(event) => {
                    setRegisterAge(event.target.value);
                  }}
                />
            </div>

          </div>


          <div class="flex justify-center items-center v-screen pt-10">
          
          <div className="mr-10">
              <label htmlFor="country" className="sr-only">
                Country
              </label>
              <input
                id="country"
                name="country"
                type="text"
                autoComplete="country"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="Country of origin"
                onChange={(event) => {
                  setRegisterCountry(event.target.value);
                }}
              />
          </div>
          <div className="ml-10">
              <label htmlFor="phone" className="sr-only">
                Phone
              </label>
              <input
                id="phone"
                name="phone"
                type="text"
                autoComplete="phone"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="Telephone number"
                onChange={(event) => {
                  setRegisterPhone(event.target.value);
                }}
              />
          </div>

        </div>


        <div class="flex justify-center items-center v-screen pt-10">
          
            <div className="mr-10">
            <label htmlFor="email-address" className="sr-only">
                  Email
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="Email"
                  onChange={(event) => {
                    setRegisterEmail(event.target.value);
                  }}
                />
            </div>
            <div className="ml-10">
                <label htmlFor="password" className="sr-only">
                  Password
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="Password"
                  onChange={(event) => {
                    setRegisterPassword(event.target.value);
                  }}
                />
            </div>

        </div>


        <div className="pt-10">
              <button
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                onClick={register}
              >
                Sign Up
              </button>
            </div>


        </div>
      </div>
    )
}

export default CreateAccountRefugee;