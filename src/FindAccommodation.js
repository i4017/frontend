import React, {useEffect,useState} from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from './firebase-config';

function FindAccommodation() {

  const navigate = useNavigate();

  const [backendData,setBackendData] = useState([{}])

    useEffect(() => {
        fetch("/offerHelpersRouter").then(
            response => response.json()
        ).then(
            data => {
              setBackendData(data)
              console.log(backendData)
            }
        )
    },[])

    const goToMainPageRefugee = async (idOffer,contactInfo) => {
      
      const contactArr = contactInfo.split(" ");

      fetch('/emailNotification/sendMail/' + contactArr[5] + '/' + "request", {  
        mode:"no-cors",
        method: 'POST', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }  
    
      })
      
      var jsonData = {
        "status" : "pending",  
      }
      fetch('/offerHelpersRouter/updateStatus/' + idOffer, {  

        method: 'PATCH', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonData) 
    
      })

      const response = await fetch('/test/getID/' + auth.currentUser.email);
      const data = await response.json();

      var jsonData2 = {
        "idOfferHelper" : idOffer,
        "status" : "pending",
        "idRefugee" : data
      }



      fetch('/offerRefugeesRouter/create_offerRefugee', {  

        method: 'POST', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonData2) 
    
      })

      const userResponse = await fetch('/test/user/' + auth.currentUser.email);
      const user = await userResponse.json();

      let refugeeContactInfo = "" + user.name + " " + user.age + " " + user.place + " " + user.phone + " " + user.email
      var jsonDataContact = {
        "refugeeContactInfo" : refugeeContactInfo,
      }

      fetch('/offerHelpersRouter/updateContactInfo/' + idOffer, {  

       method: 'PATCH', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonDataContact) 
    
      })

      navigate('/Login/MainPageRefugee')
    }

    return(
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Accommodation Offers</h2>
          </div>

          {
            (typeof backendData === 'undefined') ? 
            (<p>Loading..</p>) : 
                (
                    backendData.filter((OfferHelper) => (OfferHelper.status === 'created' && OfferHelper.type === 'accommodation')).map((OfferHelper) => (
                    <><div>
                    <div class="flex justify-center">

                      <div class="cursor-pointer content-center w-screen py-2 px-4 text-sm font-medium text-gray-900 bg-red-100 hover:bg-sky-100"
                      onClick={() => goToMainPageRefugee(OfferHelper._id,OfferHelper.helperContactInfo)}>

                        <div>
                          {"City: " + OfferHelper.city}
                        </div>

                        <div>
                          {"Maximum number of people: " + OfferHelper.maxNumber}
                        </div>

                        <div>
                          {"Maximum period of time: " + OfferHelper.period}
                        </div>

                        <div>
                          <br/>
                          {"Description:"}
                          <br/>
                        </div>

                        <div class="px-4">
                        {OfferHelper.description}
                        </div>

                      </div>

                    </div>

                               
                    
                   
                    </div></>
            ))
        )}

        </div>
      </div>
    )
}

export default FindAccommodation;