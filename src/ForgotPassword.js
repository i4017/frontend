import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { getAuth, sendPasswordResetEmail } from "firebase/auth";

function ForgotPassword() {

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const auth = getAuth();

  const resetPassword = () => {
    
    sendPasswordResetEmail(auth, email)
      .then(() => {
        alert("Email Sent!")
        goToLogin()
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorMessage)
      });
  }

  const goToLogin = () => {
    navigate('/Login')
  }

    return (
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
          <div className="max-w-md w-full space-y-8">
          <div class="px-8 mb-4 text-center">
			<h3 class="pt-4 mb-2 text-2xl">Forgot Your Password?</h3>
				<p class="mb-4 text-sm text-gray-700">
					We get it, stuff happens. Just enter your email address below and we'll send you a
					link to reset your password!
				</p>
		  </div>
            <div className="mt-8 space-y-6">
              <input type="hidden" name="remember" defaultValue="true" />
              <div className="rounded-md shadow-sm -space-y-px">
                <div>
                  <label htmlFor="email-address" className="sr-only">
                    Email
                  </label>
                  <input
                    id="email-address"
                    name="email"
                    type="email"
                    autoComplete="email"
                    required
                    className="appearance-none rounded-none relative block w-full px-3 py-2 bg-gray-200 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                    placeholder="Email"
                    onChange={(event) => {
                      setEmail(event.target.value);
                    }}
                  />
                </div>
              </div>
  
              <div>
                <button
                  onClick={resetPassword}
                  type="submit"
                  className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                  Reset Password
                </button>
              </div>
  
            </div>
          </div>
        </div>
    )
}

export default ForgotPassword;