import { useNavigate } from 'react-router-dom';
import React, {useEffect,useState } from "react";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import { auth } from "./firebase-config";


function Login() {
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const navigate = useNavigate();

  const goToForgotPassword = () => {
    navigate('ForgotPassword', { replace: false })
  }

  const goToChooseAccountType = () => {
    navigate('ChooseAccountType', { replace: false })
  }

  const gotoMainPageHelper = () => {
    navigate('MainPageHelper', { replace: false })
  }

  const gotoMainPageRefugee = () => {
    navigate('MainPageRefugee', { replace: false })
  }

  const goToMainPage = (accountType) => {
    //alert(accountType)
    if (accountType === "helper")
      gotoMainPageHelper()
    else if(accountType === "refugee")
      gotoMainPageRefugee()
  }


  const Login = async () => {

        try {
          const user =  await signInWithEmailAndPassword(
            auth,
            loginEmail,
            loginPassword
          );
      }
      catch (error) {
      console.log(error.message);
      }


    const response = await fetch('/test/' + auth.currentUser.email);
    const data = await response.json();
    goToMainPage(data)
      
    
  };

  return (
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Log In</h2>
          </div>
          <div className="mt-8 space-y-6">
            <input type="hidden" name="remember" defaultValue="true" />
            <div className="rounded-md shadow-sm -space-y-px">
              <div>
                <label htmlFor="email-address" className="sr-only">
                  Email
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 bg-gray-200 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Email"
                  onChange={(event) => {
                    setLoginEmail(event.target.value);
                  }}
                />
              </div>
              <div>
                <label htmlFor="password" className="sr-only">
                  Password
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 bg-gray-200 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Password"
                  onChange={(event) => {
                    setLoginPassword(event.target.value);
                  }}
                />
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                
              </div>

              <div className="text-sm">
                <a href="" className="font-medium text-indigo-600 hover:text-indigo-500"
                onClick={goToForgotPassword}>
                  Forgot your password?
                </a>
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                onClick={Login}>
                
                Sign In
              </button>
            </div>

            <div className="text-center">
                <a href="" className="font-medium text-indigo-600 hover:text-indigo-500"
                onClick={goToChooseAccountType}>
                  Not registered yet? Create an account
                </a>
              </div>

          </div>
        </div>
      </div>
  )
}

export default Login;