import React, {useEffect,useState} from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from './firebase-config';
import classNames from 'classnames';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";

 
function MainPageHelper() {
  const [backendData,setBackendData] = useState([{}])
  const [userId,setUserId] = useState([{}])
  const navigate = useNavigate();
    useEffect(() => {
        fetch("/offerHelpersRouter").then(
            response => response.json()
        ).then(
            data => {
              setBackendData(data)
            }
        )
    },[])

    useEffect(() => {
      fetch('/test/getID/' + auth.currentUser.email).then(
          response => response.json()
      ).then(
          data => {
            setUserId(data)
            //alert(userId)
          }
      )
  },[])

  
    
    const updateData = (OfferHelper,newStatus) => {
      document.getElementById(OfferHelper._id).style.visibility = "hidden"
      if (newStatus === "accepted")
        document.getElementById(OfferHelper._id + "ceapa").className = "flex justify-center mx-10 mb-5 bg-green-200";
      else if (newStatus === "created" || newStatus === "declined")
        document.getElementById(OfferHelper._id + "ceapa").className = "flex justify-center mx-10 mb-5 bg-yellow-200";
      else if (newStatus === "pending")
        document.getElementById(OfferHelper._id + "ceapa").className = "flex justify-center mx-10 mb-5 bg-gray-200 hover:bg-sky-100";
      
      let offerHelperStatus = "created"
      if (newStatus === "declined")
        offerHelperStatus = "created"
      else
        offerHelperStatus = newStatus

      var jsonData = {
        "status" : offerHelperStatus  
      }
      fetch('/offerHelpersRouter/updateStatus/' + OfferHelper._id, {  

        method: 'PATCH', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonData) 
    
      })

      var jsonData = {
        "status" : newStatus  
      }
      fetch('/offerRefugeesRouter/updateStatus/' + OfferHelper._id, {  

        method: 'PATCH', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonData) 
    
      })

      

      if (newStatus === "accepted" || newStatus === "declined") {

        const contactArr = OfferHelper.refugeeContactInfo.split(" ");
        //alert(contactArr[5])

        fetch('/emailNotification/sendMail/' + contactArr[5] + '/' + newStatus, {  
          mode:"no-cors",
          method: 'POST', 
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }  
      
        })
      }
     

    }
    const goToProfileHelper = () => {
        navigate('ProfileHelper', { replace: false })
      }

      const goToStart = () => {
        navigate("/");
      } 
      
      const goToMakeAccomodationOffer = () => {
        navigate('MakeAccomodationOffer', { replace: false })
      }

      const goToMakeMealOffer = () => {
        navigate('MakeMealOffer', { replace: false })
      }

      const logout = async () => {
        await signOut(auth);
        goToStart();
      };
    

    return(
        
      <div>
        <nav class="bg-white border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-800">
        <div class="container flex flex-wrap justify-between items-center mx-auto">
        <div class="flex md:order-2">
        
        <button onClick={goToProfileHelper} type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 -ml-1" viewBox="0 0 20 20" fill="currentColor">
          <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z" clip-rule="evenodd" />
        </svg>
          <p id="profileName">{auth.currentUser.email}</p>
        </button>
        
        <button onClick={logout} type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Log Out</button>
        <button data-collapse-toggle="mobile-menu-4" type="button" class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-4" aria-expanded="false">
        <span class="sr-only">Open main menu</span>
        <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
        <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
  
        
          </div>
          <div class="hidden justify-between items-center w-full md:flex md:w-auto md:order-1" id="mobile-menu-4">
          <ul class="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
          <li>
          <a href="" onClick={goToMakeAccomodationOffer} class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">Make Accommodation Offer</a>
          </li>
          <li>
          <a href="" onClick={goToMakeMealOffer} class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">Make Meal Offer</a>
          </li>
          </ul>
          </div>
          </div>
          </nav>

          <div class="grid grid-cols-2 gap-1">
          <div class="border-r-2 border-gray-400">
          <h1 class="flex justify-center py-10">Accommodation Offers</h1>
          {
            (typeof backendData === 'undefined') ? 
            (<p>Loading..</p>) : 
                (
                    backendData.filter((OfferHelper) => (OfferHelper.type === "accommodation" && OfferHelper.idHelper === userId)).map((OfferHelper) => (
                    <><div >
                    <div id={OfferHelper._id + "ceapa"} class={classNames({
                      "flex justify-center mx-10 mb-5 bg-gray-200 hover:bg-sky-100": OfferHelper.status === "pending",
                      "flex justify-center mx-10 mb-5 bg-yellow-200": OfferHelper.status === "created",
                      "flex justify-center mx-10 mb-5 bg-green-200": OfferHelper.status === "accepted",
                      
                    })}>
                    <div class="cursor-pointer content-center w-2/3 py-2 pr-4 mr-10 text-sm font-medium text-gray-900">
                        <div>
                          {"City: " + OfferHelper.city}
                        </div>

                        <div>
                          {"Maximum number of people: " + OfferHelper.maxNumber}
                        </div>

                        <div>
                          {"Maximum period of time: " + OfferHelper.period}
                        </div>

                        <div>
                          <br/>
                          {"Description:"}
                          <br/>
                        </div>

                        <div class="px-4">
                        
                        {OfferHelper.description}
                        </div>

                        {(OfferHelper.status === "pending" || OfferHelper.status === "accepted") ?
                        (<><div>
                                <br />
                                {"Contact Info:"}
                                <br />
                              </div><div class="px-4">
                                  {OfferHelper.refugeeContactInfo}
                                </div></>):
                        (<div class="px-4">
                        {""}
                      </div>)
                        }

                      </div>
                      <div id={OfferHelper._id} class={OfferHelper.status === "pending" ? "grid grid-rows-2 gap-2 py-2 visible" : "grid grid-rows-2 gap-2 py-2 invisible"}>
                        <button onClick={() => updateData(OfferHelper,"accepted")} class="bg-blue-500 text-white font-bold mb-1 px-4 rounded content-center hover:bg-red-200">
                          Accept
                        </button>
                        <button onClick={() => updateData(OfferHelper,"declined")} class="bg-blue-500 text-white font-bold mt-1 px-4 rounded content-center hover:bg-red-200">
                          Decline
                        </button>
                    </div>
                      

                    </div>              
                     
                    
                  </div></>
            ))
              )
        }
        </div>


        <div>
        <h1 class="flex justify-center py-10">Meal Offers</h1>
        {
            (typeof backendData === 'undefined') ? 
            (<p>Loading..</p>) : 
                (
                    backendData.filter((OfferHelper) => (OfferHelper.type === "meal" && OfferHelper.idHelper === userId)).map((OfferHelper) => (
                    <><div >
                    <div id={OfferHelper._id + "ceapa"} class={classNames({
                      "flex justify-center mx-10 mb-5 bg-gray-200 hover:bg-sky-100": OfferHelper.status === "pending",
                      "flex justify-center mx-10 mb-5 bg-yellow-200": OfferHelper.status === "created",
                      "flex justify-center mx-10 mb-5 bg-green-200": OfferHelper.status === "accepted",
                    })}>
                    <div class="cursor-pointer content-center w-2/3 py-2 pr-4 mr-10 text-sm font-medium text-gray-900">
                        <div>
                          {"City: " + OfferHelper.city}
                        </div>

                        <div>
                          {"Maximum number of people: " + OfferHelper.maxNumber}
                        </div>

                        <div>
                          {"Maximum period of time: " + OfferHelper.period}
                        </div>

                        <div>
                          <br/>
                          {"Description:"}
                          <br/>
                        </div>

                        <div class="px-4">
                        
                        {OfferHelper.description}
                        </div>

                        {(OfferHelper.status === "pending" || OfferHelper.status === "accepted") ?
                        (<><div>
                                <br />
                                {"Contact Info:"}
                                <br />
                              </div><div class="px-4">
                                  {OfferHelper.refugeeContactInfo}
                                </div></>):
                        (<div class="px-4">
                        {""}
                      </div>)
                        }

                      </div>
                      <div id={OfferHelper._id} class={OfferHelper.status === "pending" ? "grid grid-rows-2 gap-2 py-2 visible" : "grid grid-rows-2 gap-2 py-2 invisible"}>
                        <button onClick={() => updateData(OfferHelper,"accepted")} class="bg-blue-500 text-white font-bold mb-1 px-4 rounded content-center hover:bg-red-200">
                          Accept
                        </button>
                        <button onClick={() => updateData(OfferHelper,"declined")} class="bg-blue-500 text-white font-bold mt-1 px-4 rounded content-center hover:bg-red-200">
                          Decline
                        </button>
                    </div>
                      

                    </div>              
                     
                    
                  </div></>
            ))
              )
        }
        </div>

        </div>
          
        </div>
            )
}

export default MainPageHelper;