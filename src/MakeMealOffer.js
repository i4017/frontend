import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { auth } from './firebase-config';

function MakeMealOffer() {

    const navigate = useNavigate();

    const [offerCity, setOfferCity] = useState("");
    const [offerMaxNo, setOfferMaxNo] = useState("");
    const [offerPeriod, setOfferPeriod] = useState("");
    const [offerDescription, setOfferDescription] = useState("");

  const goToMainPageHelper = async () => {

    /*baza de date OffersHelpers
    - id
    - type
    - city
    - maxNumber
    - period
    - description
    - idHelper
    - status*/
    let type = "meal"
    let city = offerCity
    let maxNumber = offerMaxNo
    let period = offerPeriod
    let description = offerDescription
    const response = await fetch('/test/getID/' + auth.currentUser.email);
    const data = await response.json();
    let idHelper = data
    let status = "created"
    

    const userResponse = await fetch('/test/user/' + auth.currentUser.email);
    const user = await userResponse.json();
    let helperContactInfo = "" + user.name + " " + user.age + " " + user.place + " " + user.phone + " " + user.email
    
    var jsonData = {
      "type" : type,
      "city" : city,
      "maxNumber" : maxNumber,
      "period" : period,
      "description" : description,
      "idHelper" : idHelper,
      "status" : status,
      "helperContactInfo" : helperContactInfo
    }
    fetch('/offerHelpersRouter/create_offerHelper', {  

    method: 'POST', 
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(jsonData) 

  })
    
    

    navigate('/Login/ChooseAccountType/CreateAccountHelper/MainPageHelper')
  }

    return(
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Meal Offer</h2>
          </div>

          <div class="flex justify-center items-center v-screen pt-10">
          
          <div>
              <label htmlFor="city" className="sr-only">
                City
              </label>
              <input
                id="city"
                name="city"
                type="text"
                autoComplete="region"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="City"
                onChange={(event) => {
                  setOfferCity(event.target.value);
                }}
              />
          </div>

        </div>


        <div class="flex justify-center items-center v-screen">
          
          <div>
              <label htmlFor="maxnoofpeople" className="sr-only">
                Max No Of People
              </label>
              <input
                id="maxnoofpeople"
                name="maxnoofpeople"
                type="number"
                autoComplete="number"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="Max No of People"
                onChange={(event) => {
                  setOfferMaxNo(event.target.value);
                }}
              />
          </div>

        </div>



        <div class="flex justify-center items-center v-screen">
  <div>
      
    <select 
    class="rounded-none relative block w-full px-10 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200" aria-label="Default select example"
    onChange={(event) => {
      setOfferPeriod(event.target.value);
    }}>

        <option selected>Select Period</option>
        <option value="One Day">One Day</option>
        <option value="One Week">One Week</option>
        <option value="One Month">One Month</option>
        <option value="Indefinitely">Indefinitely</option>

      
        
    </select>
  </div>
</div>


<div class="justify-center items-center v-screen">
          
          <div>
              <label htmlFor="description" className="sr-only">
                Description
              </label>
              <input
                id="description"
                name="description"
                type="text"
                required
                maxLength="100"
                className="appearance-none rounded-none relative block w-full pl-3 pr-5 pt-2 pb-20 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="Description"
                onChange={(event) => {
                  setOfferDescription(event.target.value);
                }}
              />
          </div>

        </div>


        <div className="pt-10">
              <button
                onClick={goToMainPageHelper}
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                
              >
                Create Offer
              </button>
            </div>


        </div>
      </div>
    )
}

export default MakeMealOffer;