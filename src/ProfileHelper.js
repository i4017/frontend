import { useNavigate } from 'react-router-dom';
import { auth } from "./firebase-config";
import React, {useEffect,useState} from 'react';

function ProfileHelper() {

  const navigate = useNavigate();

  const [userData,setuserData] = useState({})

  const [firstAndLastName, setfirstAndLastName] = useState("");
  const [telephoneNumber, setTelephoneNumber] = useState("");
  const [city, setCity] = useState("");
  const [age, setAge] = useState("");


    useEffect(() => {
      fetch("/test/user/" + auth.currentUser.email).then(
          response => response.json()
      ).then(
          data => {
            setuserData(data)
            setfirstAndLastName(data.name)
            setTelephoneNumber(data.phone)
            setCity(data.place)
            setAge(data.age)
          }
      )
  },[])

  const goToMainPageHelper = async () => {
    /*tabela Users
      - id
      - accountType
      - name
      - age
      - place
      - phone
      - email*/
    var jsonData = {
      "name" : firstAndLastName,  
      "age" : age,
      "place" : city,
      "phone" : telephoneNumber
    }
    fetch('/offerHelpersRouter/' + auth.currentUser.email, {  

    method: 'PATCH', 
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(jsonData) 

  })
    navigate('/Login/ChooseAccountType/CreateAccountHelper/MainPageHelper')
  }
 

    return(
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Profile</h2>
          </div>

          <div class="flex justify-center items-center v-screen pt-10">
          
            <div>
                <label htmlFor="username" className="sr-only">
                  FirstLastName
                </label>
                <input
                  id="first-last-name"
                  name="name"
                  type="text"
                  value={userData.name}
                  autoComplete="name"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="First and last name"
                  onChange={(event) => {
                    userData.name = event.target.value
                    setfirstAndLastName(userData.name);
                  }}
                />
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-2 mr-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>

            <div className="ml-10">
                <label htmlFor="age" className="sr-only">
                  Age
                </label>
                <input
                  id="age"
                  name="age"
                  type="number"
                  autoComplete="age"
                  value={userData.age}
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="Age"
                  onChange={(event) => {
                    userData.age = event.target.value
                    setAge(userData.age);
                  }}
                />
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>

          </div>


          <div class="flex justify-center items-center v-screen pt-10">
          
          <div>
              <label htmlFor="city" className="sr-only">
                City
              </label>
              <input
                id="city"
                name="city"
                type="text"
                value={userData.place}
                autoComplete="region"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="City"
                onChange={(event) => {
                  userData.place = event.target.value
                  setCity(userData.place);
                }}
              />
          </div>

          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-2 mr-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>

          <div className="ml-10">
              <label htmlFor="phone" className="sr-only">
                Telephone number
              </label>
              <input
                id="phone"
                name="phone"
                type="text"
                autoComplete="phone"
                value={userData.phone}
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                placeholder="Telephone number"
                onChange={(event) => {
                  userData.phone = event.target.value
                  setTelephoneNumber(userData.phone);
                }}
              />
          </div>

          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>

        </div>


        <div class="flex justify-center items-center v-screen pt-10">
            <div>
            <label htmlFor="email-address" className="sr-only">
                  Email
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  value={userData.email}
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm bg-gray-200"
                  placeholder="Email"
                  readOnly={true}
                  disabled
                />
            </div>

        </div>


        <div className="pt-10">
              <button
                onClick={goToMainPageHelper}
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Save
              </button>
            </div>


        </div>
      </div>
    )
}

export default ProfileHelper;