import { useNavigate } from 'react-router-dom';
import React, {useEffect,useState} from 'react';
function Start() {

  const navigate = useNavigate();

  const goToLogin = () => {
    navigate('Login', { replace: false })
  }

    return(
      <section class="overflow-hidden text-gray-700">
        <div class="absolute top-7 right-10">
          <button class="py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={goToLogin}>
            Log In
          </button>
        </div>

        

        <h1 class="text-6xl font-normal leading-normal mt-0 mb-2 text-pink-800 text-center pt-20">
        Refugee Helpers
        </h1>

        <h2 class="text-3xl font-normal leading-normal mt-0 mb-2 text-pink-500 pl-20">
        Our Story
        </h2>

        <div class="container px-5 py-2 mx-auto lg:pt-12 lg:px-32">
          <div class="flex flex-wrap -m-1 md:-m-2">
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto1.jpeg')}/>
                <div class="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 flex justify-center items-end text-xl bg-gray-200 text-black font-semibold">Dwayne</div>
              </div>
            </div>
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto2.jpg')}/>
              </div>
            </div>
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto3.jpeg')}/>
              </div>
            </div>
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto4.jpeg')}/>
              </div>
            </div>
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto5.jpeg')}/>
              </div>
            </div>
            <div class="flex flex-wrap w-1/3">
              <div class="w-full p-3 md:p-2">
                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg max-w-xs hover:scale-110 transition duration-300 ease-in-out"
                  src={require('./photos/refugeephoto6.jpeg')}/>
              </div>
            </div>
          </div>
        </div>

        <p class="text-base font-light leading-relaxed p-10 ml-10 mr-10 text-justify text-pink-800">
        Since the war in Ukraine started, our mission is to help every refugee from any war zone in the world who comes to Romania accomodate and begin a new life in our country.
        Our helpers offer free food and accomodation to show our great support for everyone affected by the war in their home country. By creating an account, you will have access to
        our volunteers' offers of food or a place to stay. 
        </p>

      </section>
    );
}

export default Start;