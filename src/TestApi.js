import React, {useEffect,useState} from 'react';

function TestApi(){
    const [backendData,setBackendData] = useState([{}])

    useEffect(() => {
        fetch("/api").then(
            response => response.json()
        ).then(
            data => {
                setBackendData(data)
                
            }
        )
    },[])

    return (
        <div>
            {
            (typeof backendData.users === 'undefined') ? 
            (<p>Loading..</p>) : 
                (
                    backendData.users.map((user) => (

                    <><div>
                    <div class="flex justify-center">
                        <button type= "button" class= {user.activated === true ? "py-2 px-4 text-sm font-medium text-gray-900 bg-lime-300 hover:bg-sky-700" 
                        : "content-center py-2 px-4 text-sm font-medium text-gray-900 bg-red-600 hover:bg-sky-700"}>
                        {user.name}
                        </button>
                    </div>              
                    
                   
                    </div></>
            ))
        )}
        </div>
    )
}

export default TestApi;