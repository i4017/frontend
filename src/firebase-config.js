import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAfcFqvzzdgl4mHqSW1JlIF33dMFb-h9D8",
  authDomain: "proiectpweb.firebaseapp.com",
  projectId: "proiectpweb",
  storageBucket: "proiectpweb.appspot.com",
  messagingSenderId: "151910327012",
  appId: "1:151910327012:web:3ea697d50efce81f7c42f1",
  measurementId: "G-WNHH7DND7F"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);