import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Start from './Start';
import Login from './Login';
import ForgotPassword from './ForgotPassword';
import ChooseAccountType from './ChooseAccountType';
import CreateAccountRefugee from './CreateAccountRefugee';
import CreateAccountHelper from './CreateAccountHelper';
import ProfileRefugee from './ProfileRefugee';
import ProfileHelper from './ProfileHelper';
import TestApi from './TestApi';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
